/*
    What is a client?

    A client is an application which creates requests for resources from a server. A client will trigger an action, in the web development context, through a URL and wait for the reponse of the server.

    What is a server?
    
    A server is able to host and deliver resources requested by a client. In fact, a single server can handle multiple clients.

    What is Node.js?

    Nodejs is a runtime environment which allows us to create/develop backend/server-side applications with Javascript. Because by default, Javascript was conceptualize solely to the front end.

    Why is NodeJS popular?

    Performance - NodeJS is one of the most performing environment for creating backend applications with JS.

    Familiarity - Since NodeJS is built and uses JS as its language, it is very familiar for most developers.

    NPM (Node Package Manager) - largest registry for node packages. Packages are bits of programs, methods, functions, codes that greatly help in the development of an application.

*/

// console.log("Hello Word!");

let http = require("http");

// require() is a built-in JS method which allows us to import packages. Packages are pieces of code we can integrate into our application.

// "http" is a default package that comes with NodeJS. It allows us to use methods that let us create servers.

// http is module. Modules are packages we imported.
// Modules are objects that contains codes, methods, or data.

// The http module let us create a server which is able to communicate with a client through the use of Hypertext Transfer Protocol

// protocol to client-server communication- http://localhost:4000

// console.log(http);

http.createServer(function(request,response){
    /*
        createServer() method is a method from the http module that allows us to handle requests and responses from a client and server respectively.

        .createServer() method takes a function argument which is able to receive 2 objects. The request object which contains details of the request from the server. The response object which contains details of the response from the server. The createServer() method ALWAYS receives the request object first before the response.
    
        response.writeHead() - is a method of the response object. It allows us to add headers to our response. Headers are additional information about our response. We have 2 arguments in our writeHead() method. The first is the HTTP status code. An HTTP status is just numerical code to let th client know about the status of their request, 200, means OK, 404 means the resource cannout be found. 'Content-Type' is once of the most recongizalbe headers. It simply pertains to the data type our response.

        response.end() it ends our response. It is also able to send a message/data as a string.
    */
   /*
        Servers can actually respond differently with different requests.

        We start our requests with our URL. A client can start a different request with a different url.

        http://localhost:4000/ is not the same as http://localhost:4000/profile

        / = url endpoint (default)
        /profile = url endpoint

        we can differentiate requests by their endpoints, we should be able to respond differently to different endpoints.

        Information about the URL endpoint of the request is in the request object.

        request.url contains the URL endpoint.

        / - default endpoint - request URL: https://localhost:4000/
        /favicon.ico - Browser's default behavior to retrieve the favicon.
        /profile - request URL - https://localhost:4000/profile
        /favicon.ico

        Different requests require different responses.

        The process or way to respond differently to a request called route.

   */

    // console.log(request.url); contains the URL endpoint

    if(request.url === "/"){
        response.writeHead(200,{'Content-Type':'text/plain'});
        response.end("Hello from our first server! This is from / endpoint.");
    } else if (request.url === "/profile") {
        response.writeHead(200,{'Content-Type':'text/plain'});
        response.end("Hi! I'm Mariefher!");
    }


    

}).listen(4000);

/*
        .listen() allows us to assign a port to our server. This will allow us serve our index.js server in our local machine assigned to port 4000, There are several tasks and processes on our computer/machine that run on different port numbers.

        hypertext protocol - htttp://localhost:4000 - server
*/

console.log("Server is running on localHost:4000!");